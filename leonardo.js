const result = require('./2-movies.cjs')

const LeonardoDicaprio = () => {

    const films = {};

    for (const key in result) {

        let actorsList = result[key].actors

        actorsList.forEach(element => {
            if (element === "Leonardo Dicaprio")
                films[key] = result[key];
        });
    }

    return films;
}

module.exports = LeonardoDicaprio