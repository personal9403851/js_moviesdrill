const films = require('./2-movies.cjs')

const sortByRatings = () => {

    const result = Object.keys(films).sort((a, b) => {

        let flag = films[a].imdbRating - films[b].imdbRating

        if (flag === 0) {
            let value1 = parseFloat(films[a].totalEarnings.replace(/\$|M/g, ''))
            let value2 = parseFloat(films[b].totalEarnings.replace(/\$|M/g, ''))
            return value2 - value1;
        }

        return flag;
    })

    return result;

}

module.exports = sortByRatings