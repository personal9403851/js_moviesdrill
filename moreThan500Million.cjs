const movies = require('./2-movies.cjs')

const moreThan500 = () => {

    let million500 = {};

    for (const key in movies) {

        let earningsNumber = movies[key].totalEarnings
        earningsNumber = parseFloat(earningsNumber.replace(/\$|M/g, ''))

        if (earningsNumber > 500)
            million500[key] = movies[key]
    }
    return million500;
}

module.exports = moreThan500;