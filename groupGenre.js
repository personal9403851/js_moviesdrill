const films = require('./2-movies.cjs')

const result = () => {

    let Genre = [];

    for (const key in films) {

        let genreArray = films[key].genre

        genreArray.forEach(element => {

            if (!Genre.includes(element))
                Genre.push(element)
        });
    }

    Genre.forEach(element => {

        console.log(`Films having ${element} genre : \n`)
        for (const key in films) {
            if (films[key].genre.includes(element))
                console.log(key)
        }
    });
}

module.exports = result;