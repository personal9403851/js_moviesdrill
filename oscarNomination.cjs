const result = require('./moreThan500Million.cjs')

const moreThan500 = result();

const oscars = () => {

    let moreThan3Ooscars = {};

    for (let key in moreThan500) {

        let oscarsNumber = moreThan500[key].oscarNominations;
        
        if (oscarsNumber > 3)
            moreThan3Ooscars[key] = moreThan500[key]
    }
    return moreThan3Ooscars
}

module.exports = oscars;